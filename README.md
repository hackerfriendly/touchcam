# touchcam.py

Control a Raspberry Pi camera with a cheap LCD touch screen. Requires picamera and pygame.

# Using it

Touch the screen in the middle to save a JPG.

Touch the screen edges to pan, and the right corners to zoom.

# Hardware

An 8MP Raspberry Pi camera is assumed. It will use older 5MP cameras, but will upscale to 8MP unless you change the default cam.resolution.

Only tested on the Waveshare 4" 800x480 HDMI LCD:

https://www.waveshare.com/product/mini-pc/raspberry-pi/displays/4inch-hdmi-lcd.htm

Note that the screen will be scrambled and the touchscreen won't work on first boot unless you have this in your /boot/config.txt:

hdmi_group=2
hdmi_mode=87
hdmi_cvt 480 800 60 6 0 0 0
dtoverlay=ads7846,cs=1,penirq=25,penirq_pull=2,speed=50000,keep_vref_on=1,swapxy=0,pmax=255,xohms=150,xmin=200,xmax=3900,ymin=200,ymax=3900
display_rotate=3

You'll need to recalibrate for other displays / pen devices.

# Required software

Installing pygame and SDL is left as an exercise for the reader. Here's a starting point:

$ sudo apt-get install libsdl-dev libsdl-image1.2-dev libsdl-mixer1.2-dev libsdl-ttf2.0-dev
$ sudo apt-get install libsmpeg-dev libportmidi-dev libavformat-dev libswscale-dev
$ sudo -H pip install pygame
