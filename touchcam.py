#!/usr/bin/env python
''' 
Save a JPG every time the screen is touched. 

Touch the screen edges to pan, and the right corners to zoom.
'''
from __future__ import print_function

from time import sleep
from datetime import datetime
from picamera import PiCamera
import pygame

class camera():
    cam = PiCamera()
    # Cam can do 3280x2464
    cam.resolution = (3280, 2464)

    # Make the capture notification more visible
    cam.annotate_text_size = 128
    cam.annotate_background = True

    # Clear the screen
    screen = pygame.display.set_mode((800,480),0,32)
    pygame.mouse.set_visible(False)

    # Zoom in a little by default to help with focussing on the small display
    zoom = 30
    xpos = 30
    ypos = 40
    cam.zoom = (xpos / 100.0, ypos / 100.0, zoom / 100.0, zoom / 100.0)
    cam.start_preview()
    sleep(2)

    def update(self):
        self.zoom = max(self.zoom, 20)
        self.zoom = min(self.zoom, 100)
        self.ypos = max(self.ypos, 0)
        self.ypos = min(self.ypos, 100)
        self.xpos = max(self.xpos, 0)
        self.xpos = min(self.xpos, 100)

        print("update(", self.xpos, self.ypos, self.zoom, self.zoom, ")")
        self.cam.zoom = (self.xpos / 100.0, self.ypos / 100.0, self.zoom / 100.0, self.zoom / 100.0)

    def inc_zoom(self):
        print('inc_zoom')
        self.zoom += 5

    def dec_zoom(self):
        print('dec_zoom')
        self.zoom -= 5

    def pan_up(self):
        print('pan_up')
        self.ypos -= 5

    def pan_down(self):
        print('pan_down')
        self.ypos += 5

    def pan_left(self):
        print('pan_left')
        self.xpos -= 5

    def pan_right(self):
        print('pan_right')
        self.xpos += 5

    def capture(self):
        d = datetime.now()
        f = '{}-{:02}-{:02}.{:02}{:02}{:02}.jpg'.format(d.year, d.month, d.day, d.hour, d.minute, d.second)
        self.cam.capture(f)
        self.cam.annotate_text = f + ' saved'
        sleep(2)
        self.cam.annotate_text = None

if __name__ == '__main__':
    pygame.init()

    picam = camera()

    while True:
        ev = pygame.event.wait()
        if ev.type == pygame.MOUSEBUTTONUP:
            x, y = pygame.mouse.get_pos()
            print("x,y ==",x,y)
            # left
            if y > 300:
                if x > 200 and x < 500:
                    picam.pan_right()
            # right
            elif y < 120:
                if x < 150:
                    picam.dec_zoom()
                elif x > 250 and x < 500:
                    picam.pan_left()
                elif x > 600:
                    picam.inc_zoom()
            # top
            elif x < 150 and y > 100 and y < 300:
                picam.pan_down()
            # bottom
            elif x > 600 and y > 100 and y < 300:
                picam.pan_up()
            else:
                print('Capture!')
                picam.capture()

            picam.update()
